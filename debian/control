Source: jackson-dataformat-cbor
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 javahelper (>= 0.32),
 junit4,
 libbuild-helper-maven-plugin-java,
 libjackson2-annotations-java (>= 2.8.5),
 libjackson2-core-java (>= 2.8.5),
 libjackson2-databind-java (>= 2.8.5),
 libmaven-bundle-plugin-java,
 libmaven-enforcer-plugin-java,
 libmaven-shade-plugin-java,
 libreplacer-java,
 maven-debian-helper (>= 1.6.5),
 xmlstarlet
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/java-team/jackson-dataformat-cbor.git
Vcs-Browser: https://salsa.debian.org/java-team/jackson-dataformat-cbor
Homepage: https://github.com/FasterXML/jackson-dataformat-cbor

Package: libjackson2-dataformat-cbor
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Description: Jackson data format module for RfC7049 Concise Binary Object Representation
 The Jackson Data Processor is a multi-purpose Java library for processing
 JSON. Jackson aims to be the best possible combination of fast, correct,
 lightweight, and ergonomic for developers. It offers three alternative methods
 for processing JSON:
 .
  * Streaming API inspired by StAX
  * Tree Model
  * Data Binding converts JSON to and from POJOs
 .
 In addition to the core library, there are numerous extension that provide
 additional functionality such as additional data formats beyond JSON,
 additional data types or JVM languages.
 .
 This package contains an extension for reading and writing "Concise
 Binary Object Representation (CBOR)" as specified in RfC7049.

